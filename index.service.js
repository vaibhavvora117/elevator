function AdjustQue(queArr,fl){
    console.log(queArr.queue);
    if(queArr.elevator.currentFloor){
        var splitArr1=[];
        var splitArr2=[];
        console.log(queArr.elevator.motion);

        splitArr1=getMaxMin(queArr.queue,queArr.elevator.currentFloor,0);//max
        splitArr2=getMaxMin(queArr.queue,queArr.elevator.currentFloor,1);//min
        var isUpper=true;
         if(parseInt(fl.floor) < queArr.elevator.currentFloor){
              //splitArr2.push(parseInt(fl.floor));
              isUpper=false;
         }else{
             //splitArr1.push(parseInt(fl.floor));
             isUpper=true;
         }         
            

        
        if(queArr.elevator.motion==1){
             if(isUpper && fl.choice &&fl.choice=="up"){
                 splitArr1.push(fl.floor);                
             }else if(isUpper && fl.choice&& fl.choice=="down"){
                 splitArr2.push(fl.floor);   
             }else if(!isUpper && fl.choice && fl.choice=="down"){
                 splitArr2.push(fl.floor);
             }
         if(splitArr1 && splitArr1.length>0){
             console.log("up",splitArr1.sort(),splitArr2.sort().reverse());
             
             var fwd=splitArr1.sort();

             var res=splitArr2?splitArr2.sort().reverse():[];//4[8,9][3,2]
             if(!isUpper && fl.choice && fl.choice=="up"){
                 splitArr2.unshift(fl.floor);
             }
             console.log("up2",fwd,res);
             queArr.queue=mergeArrays(fwd,res);//4[8,9][3,2]
         }else if(splitArr2 && splitArr2.length>0){
             queArr.queue=splitArr2.sort().reverse();//4[3,2,1]

             if(!isUpper && fl.choice && fl.choice=="up"){
                 queArr.queue.unshift(fl.floor);
             }
         }
        }else if(queArr.elevator.motion==-1){
             if(!isUpper && fl.choice && fl.choice=="down"){
                 splitArr2.push(fl.floor);                
             }else if(!isUpper && fl.choice && fl.choice=="up"){
                 splitArr1.push(fl.floor);   
             }else if(isUpper && fl.choice && fl.choice=="up"){
                 splitArr1.push(fl.floor);
             }   
         if(splitArr2 && splitArr2.length>0){
             console.log("down",splitArr1.sort(),splitArr2.sort().reverse());
             
             var fwd=splitArr2.sort().reverse();
             var res=splitArr1?splitArr1.sort():[];
             console.log("down2",fwd,res);
             if(isUpper && fl.choice && fl.choice=="down"){
                 splitArr1.unshift(fl.floor);
             }
             queArr.queue=mergeArrays(fwd,res);//6[3,2][8,9]
         }else if(splitArr1 && splitArr1.length>0){
             queArr.queue=splitArr1.sort();//6[7,8,9]
             if(isUpper && fl.choice && fl.choice=="down"){
                 queArr.queue.unshift(fl.floor);
             }
         }        
        }
    } 
    
    //queArr.queue.sort();

     console.log(queArr.elevator.currentFloor,queArr.elevator.motion,"queArr",queArr.queue);
     
 }
 
 function mergeArrays(arr1,arr2){
     return arr1.concat(arr2);   
 }

 function getMaxMin(arr1,current1,choice){
     var retArray=[];
     if(choice==0){
         retArray= arr1.filter((elem)=>{
         return elem>=current1;
     });
     }else{
         retArray= arr1.filter((elem)=>{
             return elem<current1;
         });
     }
     
     console.log("a",retArray);
     return retArray;
 }
 function react(ctrl) {
     if (ctrl.queue.length == 0) return;

     if (ctrl.currentIndex == ctrl.queue[0]) {
         ctrl.elevator.motion = 0;
         ctrl.queue.shift();
         
         
         fireEvent(ctrl, "arrived");
         for(var i = 0; i < ctrl.choiceUpDownqueue.length; i++) {
             if(ctrl.choiceUpDownqueue[i].floor == ctrl.currentIndex) {
                 if(ctrl.choiceUpDownqueue[i].choice=='up' ){
                     fireEvent(ctrl, "arrivedForUp");
                 }else if(ctrl.choiceUpDownqueue[i].choice=='down'){
                     fireEvent(ctrl, "arrivedForDown");
                 }
                 ctrl.choiceUpDownqueue.splice(i, 1);
                 break;
             }
         }

         window.clearInterval(ctrl.interval);
         ctrl.interval = null;
     }
   
     if (ctrl.queue.length == 0) return;

     if (ctrl.elevator.motion) {
         ctrl.elevator.currentFloor = ctrl.floors[ctrl.currentIndex += ctrl.elevator.motion];
         fireEvent(ctrl, "floor");
         return;
     }
   
     if (ctrl.currentIndex < ctrl.queue[0]) {
         ctrl.elevator.motion = +1;
         fireEvent(ctrl, "up");
     } else if (ctrl.currentIndex > ctrl.queue[0]) {
         ctrl.elevator.motion = -1;
         fireEvent(ctrl, "down");
     }
     if (!ctrl.interval) {
         ctrl.interval = window.setInterval(function() { react(ctrl); }, ctrl.travelTime);
     }
 }

 function fireEvent(ctrl, event) {
     for (var i = 0; i < ctrl.callbacks.length; i++) {
         ctrl.callbacks[i](ctrl.elevator, event);
     }
 }