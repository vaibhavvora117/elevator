var ElevatorController = (function() {
    
    function ElevatorController(floors) {
        this.floors = floors.map(function(fl) { return fl.toString(); });
        this.travelTime = 2000;     // milliseconds from story to story
        this.currentIndex = 0;
        this.queue = [];
        this.choiceUpDownqueue=[];
        this.callbacks = [];
      
        this.elevator = {
            motion: 0,              // -1 = down, 0 = stop; 1 = up
            currentFloor: this.floors[0]
        };
    }

    
    ElevatorController.prototype.addCallback = function(callback) {
        this.callbacks.push(callback);
        return this;
    };

    ElevatorController.prototype.removeCallback = function(callback) {
        for (var i = this.callbacks.length - 1; i >= 0; i--) {
            if (this.callbacks[i] === callback) {
                this.callbacks.splice(i, 1);
            }
        }
        return this;
    };
  
   
    ElevatorController.prototype.refresh = function() {
        fireEvent(this, 'floor');
        return this;
    };

    ElevatorController.prototype.press = function(fc) {
        var fl="";
        var choiceUpDown="";
        //var fc=getFloorAndChoice(floor);
        choiceUpDown=fc.choice;
        fl=fc.floor;
        
        var index = this.floors.indexOf(fl.toString());
        if (index < 0 && choiceUpDown =="") 
        {
           // index=parseInt(fl.toString());
        return;      // Ignore invalid presses
        }
        if(choiceUpDown!==""){
            this.choiceUpDownqueue.push(fc);
        }
        if(choiceUpDown =="" ){
            this.queue.push(index);
        }else if(this.queue.length==0){
            this.queue.push(parseInt(fc.floor));
        }
        AdjustQue(this,fc);
        react(this);
        return this;
    };
    ElevatorController.prototype.getFloorAndChoice= function (fv){
        var fc={floor:"",choice:""};
        if(fv.includes('up')){
            fc.floor=parseInt(fv.split('p')[1]);
            fc.choice='up';
        }
        else if(fv.includes('down')){
            fc.floor=parseInt(fv.split('n')[1]);
            fc.choice='down';
        }    
        else{
            fc.floor=fv;
        }
        return fc;
    };
    

    return ElevatorController;
})();